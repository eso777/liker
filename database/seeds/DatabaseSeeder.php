<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(User::class, 2)->create() ;

        factory(Post::class, 3)->create(['user_id' => 1]) ;
        factory(Post::class, 3)->create(['user_id' => 2]) ;
        factory(Post::class, 3)->create(['user_id' => 1]) ;
    }
}
