<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
});

/*
 * WE DON NOT HAVE ANY POLICY RIGHT NOW SO WE WILL RETURN ALWAYS TRUE
 * TODO :: MAY BE WORK ON THIS SECTION POLICY WHO'S PERSONS CAN SEE TWEETS AND WHO'S PERSON CAN NOT SEE .
 * */
Broadcast::channel('TweetsChannel', function ($user) {
    return true;
});


Broadcast::channel('TweetsLikedChannel', function ($user) {
    return true;
});
