<?php

Route::get('/', function () { return view('welcome'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tweets', 'TweetController@index')->name('TimeLineFeeds');
Route::post('/tweets', 'TweetController@store')->name('AddTweet');
Route::post('/tweets/tweet/{tweet}/like', 'TweetLikeController@store');
