<?php

namespace App\Console\Commands;

use App\Like;
use App\Tweet;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class TruncateTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'empty:tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate Tables';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(" Okay, Lets Empty Your Tables !");

        if ($this->confirm('Do you want to Clear The Cache ?')) {
            Artisan::call('cache:clear');
            $this->info(" Cache is Cleared ...");
            Artisan::call('config:cache');
            $this->info(" Config is Cleared ...");
        }
        Tweet::query()->truncate();
        Like::query()->truncate();
        $this->info(" Done, Tweets has been Truncated Successfully !");
    }
}
