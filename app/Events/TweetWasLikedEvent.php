<?php

namespace App\Events;

use App\Tweet;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TweetWasLikedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tweet;
    public $status;

    /**
     * Create a new event instance.
     *
     * @param Tweet $tweet
     * @param int $status
     */
    public function __construct(Tweet $tweet, $status = 200)
    {
        $this->tweet = $tweet;
        $this->status = $status;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('TweetsLikedChannel');
    }
}
