<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tweet extends Model
{
    protected $fillable = ['body'];
    protected $table    = 'tweets';
    protected $appends  = ['likeCount', 'likedByCurrentUser', 'canLikedByCurrentUser', 'timeCreation'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrderLatest($query)
    {
        return $query->orderByDesc('id');
    }

    /**
     * @uses  Just to edit value for creation time to Another Format using Carbon
     * @param $value
     * @return string
     */
    public function getTimeCreationAttribute($value)
    {
        return Carbon::parse($value)->timezone('Africa/Cairo')->timestamp;
    }

    /**
     * @return mixed
     */
    public function getLikeCountAttribute()
    {
        return $this->likes->count();
    }

    /**
     * @uses CHECK IF THIS POST LIKED BY CURRENT USER OR NOT
     * @return bool
     */
    public function getLikedByCurrentUserAttribute()
    {
        // CHECK IF USER NOT LOGGED IN
        if (! Auth::check()) {
            return false;
        }

        return $this->likes->where('user_id', Auth::user()->id)->count() === 1;
    }

    /**
     * @return bool
     */
    public function getCanLikedByCurrentUserAttribute()
    {
        // CHECK IF USER NOT LOGGED IN
        if (! Auth::check()) {
            return false;
        }

        return $this->user->id !== Auth::user()->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likable');
    }

}
