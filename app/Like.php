<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';
    protected $fillable = [
        'user_id'
    ];

    public function likable()
    {
        return $this->morphTo();
    }
}



