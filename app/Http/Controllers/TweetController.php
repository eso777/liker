<?php

namespace App\Http\Controllers;

use App\Events\TweetWasCreatedEvent;
use App\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{
    /**
     * TweetController constructor.
     */
    public function __construct()
    {
       $this->middleware(['auth']);
    }

    /**
     * @param Request $request
     * @param Tweet $tweet
     * @return mixed
     */
    public function index(Request $request, Tweet $tweet)
    {
        return $tweet->with(['user'])->orderLatest()->get();
    }

    /**
     * @uses Create New Tweet With User.
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        /* VALIDATION SECTION */
        $this->validate($request, [
            'body' => 'required|string|max:500'
        ]);

        /* CREATE A NEW TWEET WITH USER SECTION */
        $tweet = $request->user()->tweets()->create([
            'body' => $request->body
        ]);

        /* BROADCAST TWEET WAS CREATE */
        broadcast(new TweetWasCreatedEvent($tweet))->toOthers();

        return $tweet;
    }
}
