<?php

namespace App\Http\Controllers;

use App\Events\TweetWasLikedEvent;
use App\Tweet;
use Illuminate\Http\Request;

class TweetLikeController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @param Request $request
     * @param Tweet $tweet
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request, Tweet $tweet)
    {
        // GET INSTANCE IF WHERE USER ID TO CHECK IF WE ADD A NEW RECORD OR DELETE CURRENT .
        $like = $tweet->likes()->firstOrNew([
            'user_id' => $request->user()->id
        ]);

        // CHECK IF CURRENT USER ALREADY LIKES THIS TWEET SO DELETE IT OTHERWISE SAVE A NEW RECORD IN LIKES TABLE
        if ($like->exists) {
            $like->delete();
            broadcast(new TweetWasLikedEvent($tweet, 201))->toOthers();
            return response(null, 201);
        }
        // SAVE A NEW LIKE RECODE FOR THIS TWEET
        $like->save();

        // START BROADCAST
        broadcast(new TweetWasLikedEvent($tweet))->toOthers();

        return response(null, 200);
    }
}
